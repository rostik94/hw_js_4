import {graph} from './graph_js.js';

const getQuantity = (data) => {
    return data.children.reduce((pre, item) => (
        item.children 
            ? pre + getQuantity(item) 
            : pre + 1
    ), 1);
};

const getSum = (data) => {
    return data.children.reduce((pre, item) => (
        item.children 
            ? pre + getSum(item) 
            : pre + item.value
    ), data.value);
};

const getNodeValue = (data, fn) => {
    return data.children.reduce((pre, item) => {
        let node = item;

        if (item.children) {
            node = getNodeValue(item, fn);
        } 

        if (fn(pre.value, node.value)) {
            return pre;
        }
        return node;
        
    }, data);
};

const maxNode = (a, b) => {
    return a > b;
};

const minNode = (a, b) => {
    return a < b;
};

console.log(getSum(graph) / getQuantity(graph));
console.log(getNodeValue(graph, maxNode));
console.log(getNodeValue(graph, minNode));